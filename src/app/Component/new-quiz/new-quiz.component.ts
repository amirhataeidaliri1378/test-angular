import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-quiz',
  templateUrl: './new-quiz.component.html',
  styleUrls: ['./new-quiz.component.css']
})
export class NewQuizComponent implements OnInit {
  title = 'Mathjax playground in Angular 6';
  answer="A"
  mathContent1 = `You can write text, that contains expressions like this: $x ^ 2 + 5$ inside them. As you probably know. You also can write expressions in display mode as follows: $$\sum_{i=1}^n(x_i^2 - \overline{x}^2)$$.`
  mathContent2 = `You can write text, that contains expressions like this: $x ^ 2 + 5$ inside them. As you probably know. You also can write expressions in display mode as follows: $$\sum_{i=1}^n(x_i^2 - \overline{x}^2)$$.`
  constructor() { }

  ngOnInit(): void {
  }

}
