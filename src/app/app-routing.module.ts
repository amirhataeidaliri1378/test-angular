import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewQuizComponent } from './Component/new-quiz/new-quiz.component';
import { TestDataComponent } from './Component/test-data/test-data.component';

const routes: Routes = [
  {
    path:'',
    component:NewQuizComponent
  },
  {
    path:'Testdata',
    component:TestDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
