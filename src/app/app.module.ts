import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MathjaxComponent } from './Component/mathjax/mathjax.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './Component/header/header.component';
import { TestDataComponent } from './Component/test-data/test-data.component';
import { NewQuizComponent } from './Component/new-quiz/new-quiz.component';

@NgModule({
  declarations: [
    AppComponent,
    MathjaxComponent,
    HeaderComponent,
    TestDataComponent,
    NewQuizComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
